<?php
  class Company { 
    public $namename; 
    public function setCompanyName($name) { $this->name = $name; } 
    public function getCompanyName() { return $this->name; }
  }

  class Department { 
    public $name; 
    public function setDepartmentName($name) { $this->name = $name; } 
    public function getDepartmentName() { return $this->name; }
  }

  class Employee{
    public $name;
    public $title;
    public $salary;
    public function setEmployeeName($name) { $this->name = $name}
    public function getEmployeeTitle() { return $this->title; }
    public function getEmployeeProfile() { return $this->name; }
    public function getEmployeeMonthlySalary() { return $this->salary; }

  }
?>